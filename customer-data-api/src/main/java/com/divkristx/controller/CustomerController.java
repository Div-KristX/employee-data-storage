package com.divkristx.controller;

import com.divkristx.model.response.CustomerResponse;
import com.divkristx.service.CustomerCRUD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("customer")
public class CustomerController {

    private static final Logger log = LoggerFactory.getLogger(CustomerController.class);
    private final CustomerCRUD customerCRUD;


    public CustomerController(CustomerCRUD customerCRUD) {
        this.customerCRUD = customerCRUD;
    }


    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public CustomerResponse getCustomerById(@PathVariable Long id) {
        log.info("Requested customer with id: {}", id);
        return customerCRUD.getCustomerById(id);
    }
}
