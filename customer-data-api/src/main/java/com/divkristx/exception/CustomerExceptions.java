package com.divkristx.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CustomerExceptions {

    public static ResponseStatusException customerNotFound(long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "Customer with id {" + id + "} not found");
    }

}
