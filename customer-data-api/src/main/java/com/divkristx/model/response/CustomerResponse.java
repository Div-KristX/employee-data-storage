package com.divkristx.model.response;

import com.divkristx.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerResponse {

    private String name;

    private String surname;

    private Integer age;

    public static CustomerResponse fromCustomer(Customer customer, Integer age) {
        return new CustomerResponse(
            customer.getName(),
            customer.getSurname(),
            age);
    }

}
