package com.divkristx.service;

import com.divkristx.model.response.CustomerResponse;

public interface CustomerCRUD {

    CustomerResponse getCustomerById(long id);
}
