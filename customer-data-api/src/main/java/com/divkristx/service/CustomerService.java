package com.divkristx.service;

import com.divkristx.exception.CustomerExceptions;
import com.divkristx.model.Customer;
import com.divkristx.model.response.CustomerResponse;
import com.divkristx.repository.CustomerRepository;
import java.time.LocalDate;
import java.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class CustomerService implements CustomerCRUD {

    private static final Logger log = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepository customerRepo;

    public CustomerService(CustomerRepository customerRepo) {
        this.customerRepo = customerRepo;
    }

    @Transactional(readOnly = true)
    @Override
    public CustomerResponse getCustomerById(long id) {
        Customer customer = getCustomer(id);
        int customerAge = getCustomerAge(customer.getBirthday());
        return CustomerResponse.fromCustomer(customer, customerAge);
    }

    private int getCustomerAge(LocalDate date) {
        return Period.between(date, LocalDate.now()).getYears();
    }

    private Customer getCustomer(long id) {
        return customerRepo
            .findById(id)
            .orElseThrow(() -> {
                log.error("Customer with id {}, not found", id);
                return CustomerExceptions.customerNotFound(id);
            });
    }
}
