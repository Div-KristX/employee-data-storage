create table customers
(
    id       bigint primary key,
    name     varchar(20) not null,
    surname  varchar(20) not null,
    birthday date        not null
);