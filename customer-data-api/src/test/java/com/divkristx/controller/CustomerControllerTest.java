package com.divkristx.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.divkristx.CustomerDataApplication;
import com.divkristx.model.Customer;
import com.divkristx.model.response.CustomerResponse;
import com.divkristx.repository.CustomerRepository;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = CustomerDataApplication.class)
@TestPropertySource(locations = "classpath:/application-db.properties")
class CustomerControllerTest {

    private final String API_URL = "/customer";
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TestRestTemplate testRestTemplate;

    @BeforeEach
    public void setUp() {
        List<Customer> customers = List.of(
            new Customer(1L, "Ann", "Wolley", LocalDate.of(2022, 1, 1)),
            new Customer(2L, "Io", "Brando", LocalDate.of(1939, 9, 29)),
            new Customer(3L, "John", "Joestar", LocalDate.of(2000, 12, 8))
        );
        customerRepository.saveAll(customers);
    }


    @Test
    public void customerWithId3IsJohnJoestar() {
        ResponseEntity<CustomerResponse> response = getResponseFromCustomerById(3L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("John", response.getBody().getName());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getAge());
        assertThat(response.getBody().getAge()).isGreaterThan(-1);
    }

    @Test
    public void customerWithId1IsAnnWolley() {
        ResponseEntity<CustomerResponse> response = getResponseFromCustomerById(1L);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Ann", response.getBody().getName());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getAge());
        assertThat(response.getBody().getAge()).isGreaterThan(-1);
    }


    @Test
    public void customerWithId5notFound() {
        ResponseEntity<CustomerResponse> response = getResponseFromCustomerById(5L);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertThat(response.getBody().getAge()).isNull();
        assertThat(response.getBody().getName()).isNull();
        assertThat(response.getBody().getSurname()).isNull();
    }


    private ResponseEntity<CustomerResponse> getResponseFromCustomerById(long id) {
        return testRestTemplate.getForEntity(API_URL + "/" + id, CustomerResponse.class,
            String.class);
    }

}
