package com.divkristx.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.divkristx.exception.CustomerExceptions;
import com.divkristx.model.Customer;
import com.divkristx.model.response.CustomerResponse;
import com.divkristx.repository.CustomerRepository;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.server.ResponseStatusException;


class CustomerServiceTest {

    private CustomerRepository customerRepo;

    private Map<Long, Customer> customers;

    private CustomerCRUD customerCRUD;

    @BeforeEach
    public void setUp() {
        customerRepo = mock(CustomerRepository.class);

        customerCRUD = new CustomerService(customerRepo);

        customers = new HashMap<>();
        customers.put(1L, new Customer(1L, "Ann", "Wolley", LocalDate.of(2022, 1, 1)));
        customers.put(2L, new Customer(2L, "Io", "Brando", LocalDate.of(1939, 9, 29)));
        customers.put(3L, new Customer(3L, "John", "Joestar", LocalDate.of(2000, 12, 8)));
    }

    @Test
    public void getCustomerWithIdFrom1to3IsCorrect() {

        when(customerRepo.findById(anyLong())).thenAnswer(
            answer -> {
                long id = answer.getArgument(0);
                return Optional.of(customers.get(id));
            }
        );

        CustomerResponse customerById1 = customerCRUD.getCustomerById(1L);
        assertThat(customerById1.getSurname()).isEqualTo("Wolley");
        assertThat(customerById1.getAge()).isGreaterThan(-1);

        CustomerResponse customerById2 = customerCRUD.getCustomerById(2L);
        assertThat(customerById2.getSurname()).isEqualTo("Brando");
        assertThat(customerById2.getName()).isEqualTo("Io");

        CustomerResponse customerById3 = customerCRUD.getCustomerById(3L);
        assertThat(customerById3).isNotNull();
    }

    @Test
    public void customerWithId4And5DoesNotExist() {
        when(customerRepo.findById(anyLong())).thenAnswer(
            answer -> {
                long id = answer.getArgument(0);
                return Optional.ofNullable(customers.get(id));
            }
        );

        assertThrows(ResponseStatusException.class, () ->
            customerCRUD.getCustomerById(4L), CustomerExceptions.customerNotFound(4L).getMessage());

        assertThrows(ResponseStatusException.class, () ->
            customerCRUD.getCustomerById(5L), CustomerExceptions.customerNotFound(5L).getMessage());

    }

}
